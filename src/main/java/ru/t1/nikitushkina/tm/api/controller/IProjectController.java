package ru.t1.nikitushkina.tm.api.controller;

public interface IProjectController {

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void clearProjects();

    void competeProjectById();

    void completeProjectByIndex();

    void createProject();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void showProjects();

    void startProjectById();

    void startProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
