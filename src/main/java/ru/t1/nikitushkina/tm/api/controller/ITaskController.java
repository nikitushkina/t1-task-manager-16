package ru.t1.nikitushkina.tm.api.controller;

public interface ITaskController {

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void clearTask();

    void competeTaskById();

    void completeTaskByIndex();

    void createTask();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void showTasks();

    void startTaskById();

    void startTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}
