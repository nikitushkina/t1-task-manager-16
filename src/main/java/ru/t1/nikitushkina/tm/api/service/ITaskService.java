package ru.t1.nikitushkina.tm.api.service;

import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    void clear();

    Task create(String name, String description);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAll(Sort sort);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task project);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
